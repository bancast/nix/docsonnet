{
  description = "docsonnet";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {self, nixpkgs, flake-utils, ...}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        # docsonnet = (pkgs.callPackage import ./default.nix {
        #   buildGoModule = pkgs.buildGoModule;
        #   lib = pkgs.lib;
        # });
        docsonnet = import ./default.nix { inherit pkgs; };
      in {
        packages.docsonnet = docsonnet;
        defaultPackage = docsonnet;
      }
    );
}
