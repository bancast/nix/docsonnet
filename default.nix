{
  pkgs ? import <nixpkgs> {},
  ...
}:
let
in
pkgs.buildGoModule rec {
  pname = "docsonnet";
  version = "0.0.6";
  src = pkgs.lib.cleanSource (builtins.fetchGit {
    url = "https://github.com/jsonnet-libs/docsonnet.git";
    rev = "503e5c8fe96d6b55775037713ac10b184709ad93";
  });

  # vendorSha256 = pkgs.lib.fakeSha256;
  vendorSha256 = "sha256-bucteWq1AZMf8i52ZPN4lzDz7euGWl4+pgRpI49Z/uY=";

  meta = with pkgs.lib; {
    description = "Generates jsonnet documentation";
    homepage = "https://github.com/jsonnet-libs/${pname}";
    license = pkgs.lib.licenses.unfree;
    maintainers = [
      "jstephenson@gitlab.com"
    ];
  };
}
